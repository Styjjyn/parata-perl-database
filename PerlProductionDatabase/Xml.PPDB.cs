﻿
using System.Xml.Serialization;
using System.IO;

namespace PerlProductionDatabase
{
	/// <summary>
    /// The configuration file for the Perl Salesforce Automator
    /// </summary>
    public class Xml_PPDB
    {
        private static string xmlPath = "Config\\Perl.ProductionDatabase.xml";

        public static Xml Load()
        {
            Xml ret = null;

            try
            {
                using (FileStream stream = File.Open(xmlPath, FileMode.Open))
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Xml));
                    ret = deserializer.Deserialize(stream) as Xml;
                    stream.Close();

                }
            }
            catch (DirectoryNotFoundException ex)
            {
                Directory.CreateDirectory(xmlPath.Split('\\')[0]);
                new Xml().Save(xmlPath);
                return Load();
            }

            catch (FileNotFoundException ex)
            {
                new Xml().Save(xmlPath);

                return Load();
            }
            

            return ret;
        }

		[XmlRoot ("Perl.ProductionDatabase")]
        public class Xml
        {
            public string databaseLocation = @"\\fserver\Shared\Manufacturing\Production\!Public\Perl\DataBases\MFG ENG ONLY - Backend\PerlProductionTracker.accdb";

            public Xml Save(string savePath)
            {
                using (FileStream stream = File.Open(savePath, FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Xml));
                    serializer.Serialize(stream, this);
                    stream.Close();
                }

                return this;
            }
        }
    }
}
