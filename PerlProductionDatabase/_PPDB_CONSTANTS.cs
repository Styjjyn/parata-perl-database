﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerlProductionDatabase
{
    /// <summary>
    /// This information must match the info in the Database
    /// </summary>
    public static class _PPDB_CONSTANTS
    {
        /// <summary>
        /// The filepath to the database
        /// </summary>
        public static string FILEPATH_DB => Xml_PPDB.Load().databaseLocation;

        /// <summary>
        /// The name of 'Variables MO Type' table
        /// </summary>
        public static string TABLENAME_VARIABLES_MOTYPE => "Variables_MO Type";

        /// <summary>
        /// The name of the 'Variables Unit Types' table
        /// </summary>
        public static string TABLENAME_VARIABLES_UNITTYPE => "Variables_Unit Types";

        #region Main table
        /// <summary>
        /// The name of the primary table
        /// </summary>
        public static string TABLENAME_MAIN => "Main";

        /// <summary>
        /// The names of the columns in the main table
        /// </summary>
        #region Column Names

        public static string COL_MAIN_CUSTOMER => "Customer";
        public static string COL_MAIN_STATUS => "Status";
        public static string COL_MAIN_UNITSN => "PerlSn";
        public static string COL_MAIN_UNITTYPE => "UnitType";
        public static string COL_MAIN_MOTYPE => "MoType";
        public static string COL_MAIN_WIN10KEY => "Win10Key";
        public static string COL_MAIN_DONGLESN => "DongleSn";
        public static string COL_MAIN_DONGLEBPCODE => "DongleBpCode";
        public static string COL_MAIN_DONGLEDVCCODE => "DongleDvcCode";
        public static string COL_MAIN_DONGLEBPREQDATE => "BpRequestDate";
        public static string COL_MAIN_DONGLEDVCREQDATE => "DvcRequestDate";
        public static string COL_MAIN_DONGLEBPRECDATE => "BpFulfillmentDate";
        public static string COL_MAIN_DONGLEDVCRECDATE => "DvcFulfillmentDate";
        public static string COL_MAIN_DONGLEBPUPGRADEREQDATE => "UpgradeRequestDate";
        public static string COL_MAIN_DONGLEBPUPGRADERECDATE => "UpgradeFulfillmentDate";
        public static string COL_MAIN_DONGLEBPPCCODE => "DongleBpPcCodes";
        public static string COL_MAIN_DVCLOCKCODE => "DvcLockCode";
        #endregion

        #endregion

    }

 
    public enum ProductionStatus
    {
        PEN_DONG_REQ,   //Pending Dongle Request
        DONG_REQD,      //Dongle has been requested
        PEN_ASLY,       //Pending Assembly
        PEN_SWIS,       //Pending Software Installation
        PEN_CALI,       //Pending Calibration
        PEN_FINF,       //Pending Final Functional
        PEN_QC,         //Pending Quality Check
        PEN_CPRP,       //Pending Crating Preparation
        PEN_CRAT,       //Pending Crating
        PEN_SHIP,       //Pending Shipment
        PEN_INST,       //Pending Installation
        INSTALLED       //Unit has been installed
    }
}
