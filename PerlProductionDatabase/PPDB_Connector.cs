﻿using System;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Collections;
using NS_Perl;
using static NS_Perl.PouchInspector;
using NS_Perl.NS_LicenseDongle;
using System.Text;
using System.IO;
using DebugLog;

namespace PerlProductionDatabase
{
    public class PPDB_Connector : IDisposable
    {

        OleDbConnection connection;
        OleDbCommand Cmd;
        public MrVishusDebugLog debugLog;

        public PPDB_Connector(MrVishusDebugLog debugLogToUse = null)
        {
            try
            {

                string connStr = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source = {_PPDB_CONSTANTS.FILEPATH_DB}";
                connection = new OleDbConnection(connStr);

                if (debugLogToUse == null)
                    debugLog = new MrVishusDebugLog("DebugLog.txt");
                else
                    debugLog = debugLogToUse;

                TestConnection();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Check to see if a connection can be made.
        /// </summary>
        /// <returns></returns>
        public bool TestConnection()
        {
            try
            {
                connection.Open();

                connection.Close();
                Console.WriteLine("Perl Database Connection Test Successfull!");
            }
            catch (Exception ex)
            {
                debugLog.AddExceptionEntry(ex, "Test Connection FAIL");

                return false;
            }


            return true;
        }

        public enum DongleRequestReturnCode
        {
            SUCCESS,
            DOWNGRADE_ATTEMPT,
            UPGRADE,
            BP_REQ_EXISTS,
            DONGLE_BP_CODE_UNSET,
            DONGLE_BP_PC_CODE_UNSET,
            DONGLE_UNSET_IN_DB,
            DONGLE_REQ_PENDING,
            DONGLE_UPG_PENDING,
            DONGLE_UPGRADED,
            DONGLE_NOT_IN_DB,
            DONGLE_ACTIVATED,
            DONGLE_ALREADY_EXISTS,
            DONGLE_BP_INIT_CODE_MATCH,
            DONGLE_TYPE_PI_TYPE_MISMATCH,
            DONGLE_CUSTOMER_ALREADY_EXISTS,
            PI_NOT_IN_DB,
            PI_WIN10_KEY_EXISTS,
            DONGLE_DVC_INIT_CODE_MATCH

        }

        public DongleRequestReturnCode GetDongleSnWithLockCode(string lockCode, out string sn)
        {

            OleDbDataReader data = GetInfoForDongleLockCode(lockCode, _PPDB_CONSTANTS.COL_MAIN_DONGLESN);
            data.Read();

            bool a = data.HasRows == false;

            if (a)
            {
                debugLog.AddFailEntry("GetDongleSnWithLockCode: No dongle found in Database with lock code: " + lockCode);
                debugLog.AddSupplement("DataReaderHasRows: " + a);

                sn = "No matching Dongle Found";
                return DongleRequestReturnCode.DONGLE_NOT_IN_DB;
            }
                

            sn = data.GetString(0);
            return DongleRequestReturnCode.SUCCESS;
        }

        public OleDbDataReader GetInfoForDongleLockCode(string dvcLockCode, string selectArgs)
        {
            OleDbDataReader ret = null;
            string cmd_GetInfo =
                $"SELECT " +
                selectArgs +
                $" FROM {_PPDB_CONSTANTS.TABLENAME_MAIN} " +
                $"WHERE {_PPDB_CONSTANTS.COL_MAIN_DVCLOCKCODE}='{dvcLockCode}'";

            if (connection.State == System.Data.ConnectionState.Closed) connection.Open();
            //Console.WriteLine($"CMD: {cmd_GetInfo}");
            Cmd = new OleDbCommand(cmd_GetInfo, connection);
            ret = Cmd.ExecuteReader();
            return ret;
        }

        public DongleRequestReturnCode GetMachineLicenseInfoForDongle(string dongleSn, out List<string> info)
        {
            OleDbDataReader read = null;
            info = new List<string>();
            DongleRequestReturnCode retCode = GetInfoForDongleSn(dongleSn, $"{_PPDB_CONSTANTS.COL_MAIN_DONGLEBPCODE}, {_PPDB_CONSTANTS.COL_MAIN_DONGLEBPPCCODE}", out read);

            if (retCode == DongleRequestReturnCode.SUCCESS)
            {

                while (read.Read())
                {
                    try
                    {
                        info.Add(read[0].ToString());
                    }
                    catch (Exception ex)
                    {
                        debugLog.AddExceptionEntry(ex, "PDDB_Connector: GetMachLicenseInfoForDongle: GetBpCode:");

                        return DongleRequestReturnCode.DONGLE_BP_CODE_UNSET;

                    }

                    try
                    {
                        info.Add(read[1].ToString());
                    }
                    catch (Exception ex)
                    {
                        debugLog.AddExceptionEntry(ex, "PDDB_Connector: GetMachLicenseInfoForDongle: GetBpPcCode:");

                        return DongleRequestReturnCode.DONGLE_BP_PC_CODE_UNSET;

                    }


                }
            }
            else
            {
                return retCode;
            }

            return DongleRequestReturnCode.SUCCESS;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unitSn"></param>
        /// <param name="selectArgs">The arguments to be used i the 'SELECT' cmd</param>
        /// <returns></returns>
        public DongleRequestReturnCode GetInfoForDongleSn(string unitSn, string selectArgs, out OleDbDataReader dbRead)
        {
            dbRead = null;
            string cmd_GetInfo =
                $"SELECT " +
                selectArgs +
                $" FROM {_PPDB_CONSTANTS.TABLENAME_MAIN} " +
                $"WHERE {_PPDB_CONSTANTS.COL_MAIN_DONGLESN}='{unitSn}'";

            if(connection.State == System.Data.ConnectionState.Closed) connection.Open();
            //Console.WriteLine($"CMD: {cmd_GetInfo}");
            Cmd = new OleDbCommand(cmd_GetInfo, connection);
            dbRead = Cmd.ExecuteReader();

            return DongleRequestReturnCode.SUCCESS;
        }

        public DongleRequestReturnCode GetInfoForPiSn(string unitSn, string selectArgs, out OleDbDataReader data)
        {
            data = null;
            string cmd_GetInfo =
                $"SELECT " +
                selectArgs +
                $" FROM {_PPDB_CONSTANTS.TABLENAME_MAIN} " +
                $"WHERE {_PPDB_CONSTANTS.COL_MAIN_UNITSN}='{unitSn}'";

            if (connection.State == System.Data.ConnectionState.Closed) connection.Open();
            //Console.WriteLine($"CMD: {cmd_GetInfo}");
            Cmd = new OleDbCommand(cmd_GetInfo, connection);
            data = Cmd.ExecuteReader();

            return DongleRequestReturnCode.SUCCESS;
        }



        public DongleRequestReturnCode UpdateDvcLockCode(string dongleSn, string lockCode)
        {
            UpdateGroup toUpdate = new UpdateGroup();
            toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DVCLOCKCODE, lockCode);

            return UpdateDongleWithSn(dongleSn, toUpdate);
        }

        public DongleRequestReturnCode UpdateDongleWithSn(string dongleSn, UpdateGroup toUpdate)
        {
            string cmd_Update = "";
            try
            {
                cmd_Update =
                $"UPDATE {_PPDB_CONSTANTS.TABLENAME_MAIN}" +
                $" SET ";

                for (int i = 0; i < toUpdate.Count; i++)
                {
                    cmd_Update += toUpdate[i];

                    if (i < toUpdate.Count - 1)
                        cmd_Update += ", ";
                }

                cmd_Update += $" WHERE " +
                    $"{_PPDB_CONSTANTS.COL_MAIN_DONGLESN} = '{dongleSn}'";

                connection.Open();
                Cmd = new OleDbCommand(cmd_Update, connection);

                Console.WriteLine("ERR: CMD: " + cmd_Update);

                Cmd.ExecuteNonQuery();
                connection.Close();

                return DongleRequestReturnCode.SUCCESS;
            }
            catch (Exception ex)
            {
                debugLog.AddExceptionEntry(ex, "UpdateDongleWithSN: " + dongleSn);
                debugLog.AddSupplement("DongleSn: " + dongleSn);
                debugLog.AddSupplement("SQL CMD Text: " + cmd_Update);
                debugLog.AddSupplement("ToUpdate: " + toUpdate.GetString());
                throw;
            }
            
        }

        public void UpdatePiWithSn(string piSn, UpdateGroup toUpdate)
        {
            string cmd_Update =
                $"UPDATE {_PPDB_CONSTANTS.TABLENAME_MAIN}" +
                $" SET ";

            for (int i = 0; i < toUpdate.Count; i++)
            {
                cmd_Update += toUpdate[i];

                if (i < toUpdate.Count - 1)
                    cmd_Update += ", ";
            }

            cmd_Update += $" WHERE " +
                $"{_PPDB_CONSTANTS.COL_MAIN_UNITSN} = '{piSn}'";

            connection.Open();
            Cmd = new OleDbCommand(cmd_Update, connection);
            Cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DongleRequestReturnCode InsertInto(InsertGroup[] inserts)
        {
            string cmd = $"INSERT INTO {_PPDB_CONSTANTS.TABLENAME_MAIN} (";
            string values = ") VALUES (";
            string valuesEnd = ")";
            for (int i = 0; i < inserts.Length; i++)
            {
                InsertGroup insert = inserts[i];
                cmd += $"{insert.columnName}";
                values += $"'{insert.value}'";

                if (i < inserts.Length - 1)
                {
                    cmd += ", ";
                    values += ", ";
                }
            }

            cmd += values;
            cmd += valuesEnd;

            //Console.WriteLine("InsertInto: Cmd: " + cmd);
            connection.Open();
            Cmd = new OleDbCommand(cmd, connection);
            Cmd.ExecuteNonQuery();
            connection.Close();
            return DongleRequestReturnCode.SUCCESS;
        }

        public DongleRequestReturnCode AddDongle(string dongleSn, UnitType unityType, MoType moType)
        {
            Perl tempInfo = new Perl
            {
                Dongle = new LicenseDongle
                {
                    Sn = dongleSn
                },
                PouchInspector = new PouchInspector
                {
                    moType = moType,
                    unitType = unityType
                }
            };

            if (DongleExists(dongleSn))
                return DongleRequestReturnCode.DONGLE_ALREADY_EXISTS;
            else
            {
                try
                {
                    InsertGroup[] newDongleInfo = new InsertGroup[]
                        {
                            new InsertGroup(_PPDB_CONSTANTS.COL_MAIN_DONGLESN, dongleSn),
                            new InsertGroup(_PPDB_CONSTANTS.COL_MAIN_MOTYPE, ((int)moType).ToString()),
                            new InsertGroup(_PPDB_CONSTANTS.COL_MAIN_UNITTYPE, ((int)unityType).ToString()),
                            new InsertGroup(_PPDB_CONSTANTS.COL_MAIN_STATUS, ((int)DongleStatus.UNSET).ToString())

                        };

                    return InsertInto(newDongleInfo);
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public DongleRequestReturnCode AddDongleRequest(string dongleSn)
        {
            DongleRequestReturnCode ret = DongleRequestReturnCode.SUCCESS;
            OleDbDataReader dataRead = null;
            GetInfoForDongleSn(dongleSn,$"{_PPDB_CONSTANTS.COL_MAIN_UNITTYPE}, {_PPDB_CONSTANTS.COL_MAIN_STATUS} ", out dataRead) ;

            if (DongleExists(dongleSn) == false)
                return DongleRequestReturnCode.DONGLE_NOT_IN_DB;

            CanDoDongleRequestResponse response;
            if (response = CanDoDongleRequest(dongleSn) == false)
            {
                return response.reason;
            }

            UpdateGroup toUpdate = new UpdateGroup(dongleSn);
            toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEBPREQDATE,DateTime.Now);
            toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEDVCREQDATE, DateTime.Now);
            toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_STATUS, (int)DongleStatus.REQUESTED_INITIAL);

            UpdateDongleWithSn(dongleSn, toUpdate);

            return ret;
        }

        public DongleRequestReturnCode AddCustomerToDongle(string dongleSn, string CustomerName)
        {
            if (GetDongleCustomerName(dongleSn) != CustomerName)
            {
                UpdateGroup toAdd = new UpdateGroup("");
                toAdd.Add(_PPDB_CONSTANTS.COL_MAIN_CUSTOMER, CustomerName);
                UpdateDongleWithSn(dongleSn, toAdd);
                return DongleRequestReturnCode.SUCCESS;
            }

            return DongleRequestReturnCode.DONGLE_CUSTOMER_ALREADY_EXISTS;
        }

        public DongleRequestReturnCode PairDongleWithPi(string piSn, UnitType piType, string dongleSn)
        {
            if (GetDongleType(dongleSn) == piType)
            {
                UpdateGroup toUpdate = new UpdateGroup("");
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_UNITSN, piSn);

                UpdateDongleWithSn(dongleSn, toUpdate);
                return DongleRequestReturnCode.SUCCESS;
            }
            else
            {
                return DongleRequestReturnCode.DONGLE_TYPE_PI_TYPE_MISMATCH;
            }
        }

        public DongleRequestReturnCode UpdateDongleBPCode_Initial(string dongleSn, string bpCode, string bpPn, bool overwrite = false)
        {

            string existing = "";
            DongleRequestReturnCode retCode = DongleRequestReturnCode.SUCCESS;
            retCode = DongleBpCodeMatches(dongleSn, bpCode, out existing);
            if (!overwrite && retCode != DongleRequestReturnCode.SUCCESS)
                return retCode;
            else
            {
                UpdateGroup toUpdate = new UpdateGroup("");
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEBPCODE, bpCode);
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEBPRECDATE, DateTime.Now);

                bool isUpgrade = bpPn.Contains("1002");

                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEBPPCCODE, isUpgrade ? 2: 1);
                if (isUpgrade)
                {
                    toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_UNITTYPE, (int) UnitType.PI_CR);
                    toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_STATUS, (int) DongleStatus.UPGRADED);
                    toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEBPUPGRADERECDATE, DateTime.Today);
                }

                UpdateDongleWithSn(dongleSn, toUpdate);
                UpdateDongleToActivated(dongleSn);
                return DongleRequestReturnCode.SUCCESS;
            }
        }

        public DongleRequestReturnCode UpdatePiWin10Key(string piSn, string key, out string existing, bool overwrite = false)
        {
            if (PiExists(piSn) == false)
            {
                debugLog.AddFailEntry($"Database: UpdatePiWin10Key: Unable to locate {piSn} in the database.");
                existing = "";
                return DongleRequestReturnCode.PI_NOT_IN_DB;
            }
                
            string a = PiHasWin10Key(piSn);

            if (a == key)
            {
                existing = "";
                return DongleRequestReturnCode.SUCCESS;
            }
                

            if (overwrite == false && a != "")
            {
                debugLog.AddFailEntry($"Database: UpdatePiWin10Key: PI SN: {piSn} has another key.");
                debugLog.AddSupplement(a);
                existing = a;
                return DongleRequestReturnCode.PI_WIN10_KEY_EXISTS;
            }
                

            UpdateGroup toAdd = new UpdateGroup("");
            toAdd.Add(_PPDB_CONSTANTS.COL_MAIN_WIN10KEY, key);

            UpdatePiWithSn(piSn, toAdd);
            existing = "";
            return DongleRequestReturnCode.SUCCESS;
        }

        public DongleRequestReturnCode UpdateDongleDvcCode_Initial(string dongleSn, string dvcCode, bool overwrite = false)
        {
            string s = "";
            DongleRequestReturnCode retCode = DongleRequestReturnCode.SUCCESS;
            retCode = DongleDvcCodeMatches(dongleSn, dvcCode, out s);
            if (!overwrite && retCode == DongleRequestReturnCode.DONGLE_DVC_INIT_CODE_MATCH)
            {
                debugLog.AddFailEntry("UpdateDongleDvcCode: Found another DVC Code for Dongle: " + dongleSn);
                debugLog.AddSupplement(s);
                return DongleRequestReturnCode.DONGLE_DVC_INIT_CODE_MATCH;
            }
                
            else if(s != dvcCode)
            {
                UpdateGroup toUpdate = new UpdateGroup("");
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEDVCCODE, dvcCode);
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_DONGLEDVCRECDATE, DateTime.Now);
                UpdateDongleWithSn(dongleSn, toUpdate);
                UpdateDongleToActivated(dongleSn);
                
            }

            return DongleRequestReturnCode.SUCCESS;
        }

        public bool DongleExists(string unitSn)
        {
            bool ret = false;
            OleDbDataReader data = null;
            GetInfoForDongleSn(unitSn, "* ", out data);

            using (data)
            {
                ret = data.HasRows;
                connection.Close();
            }

            return ret;
        }

        public bool PiExists(string unitSn)
        {
            bool ret = false;
            OleDbDataReader data = null;
            GetInfoForPiSn(unitSn, "* ", out data);

            using (data)
            {
                ret = data.HasRows;
                connection.Close();
            }

            return ret;
        }

        public string PiHasWin10Key(string unitSn)
        {

            string ret = "";
            OleDbDataReader data = null;
            GetInfoForPiSn(unitSn, $"{_PPDB_CONSTANTS.COL_MAIN_WIN10KEY} ", out data);

            using (data)
            {
                data.Read();

                if (IsDbNull(data[0]))
                    ret = "";
                else
                {
                    ret = data.GetString(0);
                }
                connection.Close();
            }

            return ret;

        }

        public DongleRequestReturnCode DongleBpCodeMatches(string dongleSn, string bpCode, out string existing)
        {
            DongleRequestReturnCode retCode = DongleRequestReturnCode.SUCCESS;
            OleDbDataReader data = null;
            GetInfoForDongleSn(dongleSn, _PPDB_CONSTANTS.COL_MAIN_DONGLEBPCODE, out data);
            using (data)
            {
                data.Read();
                if (data[0].GetType() == typeof(DBNull))
                {
                    existing = "";
                    retCode = DongleRequestReturnCode.SUCCESS;
                }

                else
                {
                    existing = data.GetString(0);
                    retCode = existing != bpCode ? DongleRequestReturnCode.DONGLE_BP_INIT_CODE_MATCH : retCode;
                }
                    

                connection.Close();
            }

            return retCode;
        }


        public DongleRequestReturnCode DongleDvcCodeMatches(string dongleSn, string dvcCode, [Optional]out string foundSn)
        {

            OleDbDataReader data = null;
            foundSn = "NULL";

            DongleRequestReturnCode retCode =  GetInfoForDongleSn(dongleSn, _PPDB_CONSTANTS.COL_MAIN_DONGLEDVCCODE, out data);

            if (retCode != DongleRequestReturnCode.SUCCESS)
                return retCode;

            using (data)
            {
                data.Read();
                if (data[0].GetType() == typeof(DBNull))
                {
                    retCode = DongleRequestReturnCode.SUCCESS;
                }
                    
                else
                {
                    foundSn = data.GetString(0);
                    bool b = foundSn != dvcCode;

                    if (b)
                        retCode = DongleRequestReturnCode.DONGLE_DVC_INIT_CODE_MATCH;
                }
                    

                connection.Close();
            }

            return retCode;
        }

        public string GetDongleCustomerName(string dongleSn)
        {
            string ret = "";

            OleDbDataReader data = null;
            GetInfoForDongleSn(dongleSn, _PPDB_CONSTANTS.COL_MAIN_CUSTOMER, out data);

            using (data)
            {
                data.Read();

                if (IsDbNull(data[0]))
                    ret = "";
                else
                    ret = data.GetString(0);

                connection.Close();
            }

            return ret;
        }

        public string GetPiCustomerName(string piSn)
        {
            string ret = "";
            OleDbDataReader data = null;
            GetInfoForPiSn(piSn, _PPDB_CONSTANTS.COL_MAIN_CUSTOMER, out data);

            using (data)
            {
                data.Read();

                if (IsDbNull(data[0]))
                    ret = "";
                else
                    ret = data.GetString(0);

                connection.Close();
            }

            return ret;
        }

        public UnitType GetDongleType(string dongleSn)
        {
            UnitType ret = UnitType.UNSET;
            OleDbDataReader data = null;
            GetInfoForDongleSn(dongleSn, _PPDB_CONSTANTS.COL_MAIN_UNITTYPE, out data);

            using (data)
            {
                data.Read();

                if (IsDbNull(data[0]))
                    ret = UnitType.UNSET;
                else
                    ret = (UnitType)data.GetInt32(0);

                connection.Close();
            }

            return ret;
        }

        public bool DongleActivatable(string dongleSn)
        {
            bool ret = false;
            OleDbDataReader data = null;
            GetInfoForDongleSn(dongleSn, _PPDB_CONSTANTS.COL_MAIN_STATUS + ", " + _PPDB_CONSTANTS.COL_MAIN_DONGLEBPCODE + ", " + _PPDB_CONSTANTS.COL_MAIN_DONGLEDVCCODE, out data);


            using (data)
            {
                data.Read();

                if (IsDbNull(data[1]) || IsDbNull(data[2]))
                    ret = false;
                else if (IsDbNull(data[0]))
                    ret = true;
                else
                    ret = (DongleStatus)data.GetInt32(0) == DongleStatus.REQUESTED_INITIAL;

                connection.Close();
            }

            return ret;
        }

        private bool IsDbNull(Object o)
        {
            return o.GetType() == typeof(DBNull);
        }

        

        public void UpdateDongleToActivated(string dongleSn)
        {
            if (DongleActivatable(dongleSn))
            {
                UpdateGroup toUpdate = new UpdateGroup("");
                toUpdate.Add(_PPDB_CONSTANTS.COL_MAIN_STATUS, (int)DongleStatus.ACTIVATED);

                UpdateDongleWithSn(dongleSn, toUpdate);

            }

        }

        

        public CanDoDongleRequestResponse CanDoDongleRequest(string unitSn)
        {
            CanDoDongleRequestResponse ret = new CanDoDongleRequestResponse();
            OleDbDataReader data = null;
            GetInfoForDongleSn(unitSn, $"{_PPDB_CONSTANTS.COL_MAIN_STATUS}", out data);

            using (data)
            {
                data.Read();
                switch ((DongleStatus)data.GetInt32(0))
                {
                    case DongleStatus.UNSET:
                        ret = true;
                        break;
                    case DongleStatus.PAIRED:
                        ret = false;
                        ret.reason = DongleRequestReturnCode.DONGLE_ACTIVATED;
                        break;
                    case DongleStatus.REQUESTED_INITIAL:
                        ret = false;
                        ret.reason = DongleRequestReturnCode.DONGLE_REQ_PENDING;
                        break;
                    case DongleStatus.REQUESTED_UPGRADE:
                        ret = false;
                        ret.reason = DongleRequestReturnCode.DONGLE_UPG_PENDING;
                        break;
                    case DongleStatus.UPGRADED:
                        ret = false;
                        ret.reason = DongleRequestReturnCode.DONGLE_UPGRADED;
                        break;

                }
                connection.Close();
            }

            return ret;
        }

        

        public struct CanDoDongleRequestResponse
        {
            public bool result;
            public DongleRequestReturnCode reason;

            public static bool operator ==(CanDoDongleRequestResponse a, bool b)
            {
                return a.result == b;
            }

            public static bool operator !=(CanDoDongleRequestResponse a, bool b)
            {
                return a.result != b;
            }

            public static implicit operator bool(CanDoDongleRequestResponse a)
            {
                return a.result;
            }

            public static implicit operator CanDoDongleRequestResponse(bool a)
            {
                CanDoDongleRequestResponse ret = new CanDoDongleRequestResponse();
                ret.result = a;
                return ret;
            }
        }

        public struct InsertGroup
        {
            public string columnName, value;

            public InsertGroup(string column, string v)
            {
                columnName = column;
                value = v;
            }
        }

        public struct UpdateGroup : ICollection<UpdateGroup.Entry>
        {
            private List<Entry> list;

            public int Count => list.Count;

            public bool IsReadOnly => ((ICollection<Entry>)list).IsReadOnly;

            public Entry this[int index]
            {
                get => list[index];
                set => list[index] = value;
            }

            public UpdateGroup(string s)
            {
                list = new List<Entry>();
            }

            public string GetString()
            {
                string ret = "";

                foreach (Entry e in list)
                {
                    ret += e.columnName + "=" + e.value + ";";
                }

                return ret;
            }

            public struct Entry
            {
                public string columnName, value;

                public Entry(string c, string v)
                {
                    columnName = c;
                    value = v;
                }

                

                public static implicit operator string(Entry a)
                {
                    return $"{a.columnName} = '{a.value}'";
                }
            }

            public void Add(string column, object o)
            {
                Add(column, o.ToString());
            }

            public void Add(string column, string v)
            {
                Add(new Entry(column,v));
            }

            public void Add(Entry item)
            {
                ((ICollection<Entry>)list).Add(item);
            }

            public void Clear()
            {
                ((ICollection<Entry>)list).Clear();
            }

            public bool Contains(Entry item)
            {
                return ((ICollection<Entry>)list).Contains(item);
            }

            public void CopyTo(Entry[] array, int arrayIndex)
            {
                ((ICollection<Entry>)list).CopyTo(array, arrayIndex);
            }

            public bool Remove(Entry item)
            {
                return ((ICollection<Entry>)list).Remove(item);
            }

            public IEnumerator<Entry> GetEnumerator()
            {
                return ((ICollection<Entry>)list).GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return ((ICollection<Entry>)list).GetEnumerator();
            }

            
        }


        public void Dispose()
        {
            if (connection.State == System.Data.ConnectionState.Open)
                connection.Close();
        }
    }
}
