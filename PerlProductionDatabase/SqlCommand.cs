﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerlProductionDatabase
{
    public abstract class SqlCommand
    {
        public abstract string GetStatement();
    }

    public class SqlClause_Where
    {
        public string criteria;

        public string GetStatement()
        {
            string ret = "WHERE " + criteria;

            return ret;
        }

    }

    public class SqlCommand_InsertInto : SqlCommand
    {
        public string tableName;
        public ColumnValuePair[] columnValuePairs;

        public override string GetStatement()
        {
            string stmt = "INSERT INTO ";
            string a = "VALUES (";
            if (columnValuePairs != null && columnValuePairs.Length > 0)
            {
                for (int i = 0; i < columnValuePairs.Length; i++)
                {
                    ColumnValuePair pair = columnValuePairs[i];

                    if (pair.columnName != null && pair.columnName != "")
                    {
                        if (i == 0)
                            stmt += "(";

                        stmt += pair.columnName;

                        if (i != columnValuePairs.Length - 1)
                            stmt += ", ";
                        else
                            stmt += ") ";

                    }

                    if (pair.value != null)
                    {
                        a += "'" + pair.value + "'";

                        if (i != columnValuePairs.Length - 1)
                            a += ", ";
                        else
                            a += ")";
                    }

                }
            }
            stmt += a;
            return stmt;
        }

        

    }

    

    public class ColumnValuePair
    {
        public string columnName;
        public object value;
        public Type valueType;

        public ColumnValuePair(string c, object v, Type t)
        {
            columnName = c;
            value = v;
            valueType = t;
        }
    }

    public class SqlCommand_Select : SqlCommand
    {
        public string[] columnNames;
        public string tableName = "";
        /// <summary>
        /// [OPTIONAL]
        /// </summary>
        public SqlClause_Where where;

        public override string GetStatement()
        {
            string stmt = "SELECT ";

            for (int i = 0; i < columnNames.Length; i++)
            {
                stmt += columnNames[i];

                if (i < columnNames.Length - 1)
                    stmt += ", ";
                else
                    stmt += " ";
            }

            stmt += "FROM " + tableName;

            if (where != null)
            {
                stmt += " " + where.GetStatement();
            }

            return stmt;
        }
    }
}
